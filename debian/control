Source: yakuake
Section: kde
Priority: optional
Maintainer: Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Aurélien COUDERC <coucouf@debian.org>,
           Norbert Preining <norbert@preining.info>,
           Scarlett Moore <sgmoore@kde.org>,
           Didier Raboud <odyx@debian.org>,
Build-Depends: cmake (>= 3.16~),
               debhelper-compat (= 13),
               extra-cmake-modules (>= 5.90~),
               gettext,
               libkf5archive-dev (>= 5.90~),
               libkf5config-dev (>= 5.90~),
               libkf5coreaddons-dev (>= 5.90~),
               libkf5crash-dev (>= 5.90~),
               libkf5dbusaddons-dev (>= 5.90~),
               libkf5globalaccel-dev (>= 5.90~),
               libkf5i18n-dev (>= 5.90~),
               libkf5iconthemes-dev (>= 5.90~),
               libkf5kio-dev (>= 5.90~),
               libkf5newstuff-dev (>= 5.90~),
               libkf5notifications-dev (>= 5.90~),
               libkf5notifyconfig-dev (>= 5.90~),
               libkf5parts-dev (>= 5.90~),
               libkf5wayland-dev (>= 4:5.90~) [linux-any],
               libkf5widgetsaddons-dev (>= 5.90~),
               libkf5windowsystem-dev (>= 5.90~),
               libqt5svg5-dev (>= 5.15.0~),
               libqt5x11extras5-dev (>= 5.15.0~),
               libx11-dev,
               pkg-kde-tools (>= 0.4.2),
               qtbase5-dev (>= 5.15.0~),
Standards-Version: 4.6.1
Homepage: https://apps.kde.org/en/yakuake
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/yakuake.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/yakuake
Rules-Requires-Root: no

Package: yakuake
Architecture: any
Depends: konsole-kpart, ${misc:Depends}, ${shlibs:Depends}
Provides: x-terminal-emulator
Description: Quake-style terminal emulator based on KDE Konsole technology
 YaKuake is inspired from the terminal in the Quake game: when you press a key
 (by default F12, but that can be changed) a terminal window slides down from
 the top of the screen. Press the key again, and the terminal slides back.
 .
 It is faster than a keyboard shortcut because it is already loaded into memory
 and as such is very useful to anyone who frequently finds themselves switching
 in and out of terminal sessions.
